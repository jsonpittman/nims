#INCLUDE "OPTS.BAS"
#include "TUI.BAS"
#include ONCE "BOX.BAS"

Declare Function SelectedBoxID() As Integer
Declare Function SelectedItemID() As Integer
Declare Function NextItemID(SelID As Integer) As Integer
Declare Function PreviousItemID(NextID As Integer) As Integer
Declare Sub SelectFirstItem()
Declare Sub DisplayBoxes(bb() As BoxDat, Focused As Boolean)
Declare Sub DisplayItems(ii() As ItemDat, BoxID As Integer, Focused As Boolean)

Dim Shared BoxDats() As BoxDat
Dim Shared ItemDats() As ItemDat
Dim Shared As String SearchTerm
Dim As Integer SelectedArea
Dim As Long key
Dim EditVisible As Boolean
EditVisible = False

SelectedArea = 1

LoadOptionsFile("NIMS.DAT", OptionLines())

TUI.BackgroundColor = Val(loadOption("BackgroundColor", OptionLines()))
TUI.ForegroundColor = Val(loadOption("ForegroundColor", OptionLines()))
TUI.BorderColor = Val(loadOption("BorderColor", OptionLines()))
TUI.TitleColor = Val(loadOption("TitleColor", OptionLines()))
TUI.SearchMatchColor = Val(loadOption("SearchMatchColor", OptionLines()))
TUI.BoxTextColor = Val(loadOption("BoxTextColor", OptionLines()))
TUI.TL_CornerChar = Val( loadOption("TL_CornerChar", OptionLines()))
TUI.TR_CornerChar = Val(loadOption("TR_CornerChar", OptionLines()))
TUI.BL_CornerChar = Val(loadOption("BL_CornerChar", OptionLines()))
TUI.BR_CornerChar = Val(loadOption("BR_CornerChar", OptionLines()))
TUI.VertBarChar = Val(loadOption("VertBarChar", OptionLines()))
TUI.HorizBarChar = Val(loadOption("HorizBarChar", OptionLines()))

Boxes.Title = loadOption("Boxes.Title", OptionLines())
Boxes.Top = Val(loadOption("Boxes.Top", OptionLines()))
Boxes.Left = Val(loadOption("Boxes.Left", OptionLines()))
Boxes.HeightPerc = Val(loadOption("Boxes.HeightPerc", OptionLines()))
Boxes.WidthPerc = Val(loadOption("Boxes.WidthPerc", OptionLines()))

BoxDescrip.Title = loadOption("BoxDescrip.Title", OptionLines())
BoxDescrip.Top = Val(loadOption("BoxDescrip.Top", OptionLines()))
BoxDescrip.Height = Val(loadOption("BoxDescrip.Height", OptionLines()))

BoxItems.Title = loadOption("BoxItems.Title", OptionLines())

BoxSearch.Title = loadOption("BoxSearch.Title", OptionLines())
BoxSearch.Height = Val(loadOption("BoxSearch.Height", OptionLines()))

BoxEdit.Title = "Edit"
BoxEdit.Width = Loword(Width()) *.8
BoxEdit.Left = (Loword(Width()) -  BoxEdit.Width) / 2
BoxEdit.Height = Hiword(Width()) *.8
BoxEdit.Top = (Hiword(Width()) - BoxEdit.Height) / 2

CalculateLayout(Boxes, BoxDescrip, BoxItems, BoxSearch)

Cls

DrawBox(Boxes, TUI)
DrawBox(BoxDescrip, TUI)
DrawBox(BoxItems, TUI)
DrawBox(BoxSearch, TUI)

REM TEST Data
'TODO: load from file
AddBox(BoxDats(), "Brown Shoebox", "This old brown shoebox contains computer cables, a few old hard drives, and some cat toys.")
AddBox(BoxDats(), "Orange Crate", "This is an orange crate.")
AddBox(BoxDats(), "Green Box", "This is a green box.")
AddBox(BoxDats(), "Small Clear Box #1", "This is a small clear box on the shelf in my office")
AddBox(BoxDats(), "Small Clear Box #2", "This is a small clear box on the shelf in my office")
AddBox(BoxDats(), "Small Clear Box #3", "This is a small clear box on the shelf in my office")
AddBox(BoxDats(), "Small Clear Box #4", "This is a small clear box on the shelf in my office")
AddBox(BoxDats(), "Small Clear Box #5", "This is a small clear box on the shelf in my office")

AddItem(ItemDats(), 1, 3, "07/08/23", "USB Cables", false, false)
AddItem(ItemDats(), 1, 1, "07/08/23", "Old Hard Drives", false, false)
AddItem(ItemDats(), 1, 2, "07/08/23", "Cat Toys", false, false)

AddItem(ItemDats(), 2, 3, "07/08/23", "Phone Cords", false, false)
AddItem(ItemDats(), 2, 1, "07/08/23", "Old Cell Phones", false, false)

AddItem(ItemDats(), 3, 3, "07/08/23", "Ethernet Cables", false, false)
AddItem(ItemDats(), 3, 1, "07/08/23", "Old Routers", false, false)

AddItem(ItemDats(), 4, 3, "07/08/23", "USB Cables", false, false)
AddItem(ItemDats(), 4, 1, "07/08/23", "Old Hard Drives", false, false)
AddItem(ItemDats(), 4, 2, "07/08/23", "Toothbrush Collection", false, false)

AddItem(ItemDats(), 5, 2, "07/08/23", "Bumper Stickers", false, false)
AddItem(ItemDats(), 5, 3, "07/08/23", "USB Cables", false, false)
AddItem(ItemDats(), 5, 1, "07/08/23", "Old Hard Drives", false, false)

AddItem(ItemDats(), 6, 3, "07/08/23", "Zip Disks", false, false)
AddItem(ItemDats(), 6, 1, "07/08/23", "Floppy disks", false, false)
AddItem(ItemDats(), 6, 2, "07/08/23", "iPod Touch", false, false)
REM End TEST Data

' select first box
For BoxStep As Integer = 1 To Ubound(BoxDats)
    If BoxStep = 1 Then
        BoxDats(BoxStep).Selected = 1
    Else
        BoxDats(BoxStep).Selected = 0
    End If
Next

For itemStep As Integer = Lbound(ItemDats) To Ubound(ItemDats)
    If itemStep = Lbound(ItemDats) Then
        ItemDats(itemStep).Selected = 1
    Else
        ItemDats(itemStep).Selected = 0
    End If
Next

DisplayBoxes(BoxDats(), True)
DisplayItems(ItemDats(), SelectedBoxID(), False)

Do
    key = Getkey
    ' print key
    ' end
    Select Case key
    Case 13 'enter
        If SelectedArea = 1 Then
            DrawBox(BoxEdit, TUI)
            ClearLayout(BoxEdit)
            LayoutText("** This doesn't Work...Press 'x' to close **", False, 0, BoxEdit, TextMode.CENTER, False, False, SearchTerm)
            LayoutText("Box Title:", False, 2, BoxEdit, TextMode.CENTER, False, False, SearchTerm)
            LayoutText(BoxDats(SelectedBoxID).Title, False, 3, BoxEdit, TextMode.CENTER, False, False, SearchTerm)
            LayoutText("Box Description:", False, 5, BoxEdit, TextMode.CENTER, False, False, SearchTerm)
            LayoutText(BoxDats(SelectedBoxID).Description, False, 6, BoxEdit, TextMode.CENTER, False, False, SearchTerm)
            Locate BoxEdit.Top+1, BoxEdit.Left+2
            EditVisible = True
        ElseIf SelectedArea = 2 Then
            DrawBox(BoxEdit, TUI)
            ClearLayout(BoxEdit)
            LayoutText("** This doesn't Work...Press 'x' to Close **", False, 0, BoxEdit, TextMode.CENTER, False, False, SearchTerm)
            LayoutText("Item Title:", False, 2, BoxEdit, TextMode.CENTER, False, False, SearchTerm)
            LayoutText(ItemDats(SelectedItemID).Title, False, 3, BoxEdit, TextMode.CENTER, False, False, SearchTerm)
            LayoutText("Item Description:", False, 5, BoxEdit, TextMode.CENTER, False, False, SearchTerm)
            LayoutText(ItemDats(SelectedItemID).Description, False, 6, BoxEdit, TextMode.CENTER, False, False, SearchTerm)
            Locate BoxEdit.Top+1, BoxEdit.Left+2
            EditVisible = True
        End If
    Case 9 'Tab
        if EditVisible = False Then
            SelectedArea = selectedArea + 1
            If SelectedArea > 3 Then
                SelectedArea = 1
            End If
            
            Select Case selectedArea
            Case 1
                DisplayBoxes(BoxDats(), True)
                DisplayItems(ItemDats(), SelectedBoxID(), False)
            Case 2
                DisplayBoxes(BoxDats(), False)
                If SelectedItemID() = -1 Then
                    SelectFirstItem()
                End If
                DisplayItems(ItemDats(), SelectedBoxID(), True)
            Case 3
                DisplayBoxes(BoxDats(), False)
                DisplayItems(ItemDats(), SelectedBoxID(), False)
                Locate BoxSearch.Top+1, BoxSearch.Left+2
            End Select
        End If
    Case 20735 'Down arrow
        if EditVisible = False Then
            If SelectedArea = 1 Then
                For BoxStep As Integer = 1 To Ubound(BoxDats)
                    If BoxDats(UBound(BoxDats)).Selected = False Then
                        If BoxDats(BoxStep).Selected = True Then
                            BoxDats(BoxStep).Selected = False
                            BoxDats(BoxStep+1).Selected = True
                            Exit For
                        End If
                    End If
                Next
                DisplayBoxes(BoxDats(), True)
                DisplayItems(ItemDats(), SelectedBoxID(), False)
            Elseif SelectedArea = 2 Then
                Dim As Integer selID
                Dim As Integer NextID

                selID = SelectedItemID()
                NextID = NextItemID(selID)

                if (SelID > -1 And NextID > -1) Then
                    For ItemStep As Integer = Lbound(ItemDats) To Ubound(ItemDats)
                        If ItemDats(ItemStep).ID = SelectedItemID Then
                            ItemDats(ItemStep).Selected = False
                        End If
                        If ItemDats(ItemStep).ID = NextID Then
                            ItemDats(ItemStep).Selected = True
                            exit for
                        End If
                    Next
                End If
    
                DisplayItems(ItemDats(), SelectedBoxID(), True)
            End If
        End If
    Case 19455 'left arrow
        'TODO: Do something
    Case 19967 'right arrow
        'TODO: Do something
    Case 18687 'up arrow
        If EditVisible = False Then
            If SelectedArea = 1 Then
                If(BoxDats(1).Selected = False) Then
                    For BoxStep As Integer = 1 To Ubound(BoxDats)
                        If BoxDats(BoxStep).Selected = True Then
                            BoxDats(BoxStep).Selected = False
                            BoxDats(BoxStep-1).Selected = True
                            Exit For
                        End If
                    Next
                End If
                DisplayBoxes(BoxDats(), True)
                DisplayItems(ItemDats(), SelectedBoxID(), False)
            ElseIf SelectedArea = 2 Then
                Dim As Integer selID
                Dim As Integer prevID
                
                selID = SelectedItemID()
                prevID = PreviousItemID(selID)

                if (SelID > -1 And prevID > -1) Then
                    For ItemStep As Integer = Ubound(ItemDats) To Lbound(ItemDats) Step -1
                        If ItemDats(ItemStep).ID = SelectedItemID Then
                            ItemDats(ItemStep).Selected = False
                        End If
                        If ItemDats(ItemStep).ID = prevID Then
                            ItemDats(ItemStep).Selected = True
                            exit for
                        End If
                    Next
                End If
                DisplayItems(ItemDats(), SelectedBoxID(), True)
            End If
        End If
    case Else
        ' print key
        ' end
        if key = 120 And EditVisible = True then 'x
                ClearScreen()
                DrawBox(Boxes, TUI)
                DrawBox(BoxDescrip, TUI)
                DrawBox(BoxItems, TUI)
                DrawBox(BoxSearch, TUI)
                EditVisible = False
        elseif key = 8 then 'backspace
            if len(SearchTerm) > 0 then
                SearchTerm = Left(SearchTerm, Len(SearchTerm)-1)
            end if
        else
            if key >= 32 and key <= 126 then
                SearchTerm = SearchTerm & Chr(key)
            end if
        end if
        Locate BoxSearch.Top+1, BoxSearch.Left+2
        print String(BoxSearch.Width - 4, " ")
        Locate BoxSearch.Top+1, BoxSearch.Left+2
        print SearchTerm;
        DisplayBoxes(BoxDats(), False)
        DisplayItems(ItemDats(), SelectedBoxID(), False)
    End Select
Loop Until key = 27

' TODO: write changes on exit
Cls
End

Function SelectedBoxID() As Integer
    For BoxStep As Integer = 1 To Ubound(BoxDats)
        If BoxDats(BoxStep).Selected = True Then
            Return BoxDats(BoxStep).ID
        End If
    Next
End Function

Function SelectedItemID() As Integer
    For ItemStep As Integer = Lbound(ItemDats) To Ubound(ItemDats)
        If ItemDats(ItemStep).BoxID = SelectedBoxID And ItemDats(ItemStep).Selected = True Then
            If ItemDats(ItemStep).BoxID = SelectedBoxID() Then
                Return ItemDats(ItemStep).ID
            End If
        End If
    Next
    Return -1
End Function

Function NextItemID(SelID As Integer) As Integer
    For ItemStep As Integer = Lbound(ItemDats) To Ubound(ItemDats)
        If ItemDats(ItemStep).BoxID = SelectedBoxID Then
            If ItemDats(ItemStep).ID > selID Then
                Return ItemDats(ItemStep).ID
            End If
        End If
    Next
    Return -1
End Function

Function PreviousItemID(NextID As Integer) As Integer
    For ItemStep As Integer = Ubound(ItemDats) To Lbound(ItemDats) Step -1
        If ItemDats(ItemStep).BoxID = SelectedBoxID Then
            If ItemDats(ItemStep).ID < NextID Then
                Return ItemDats(ItemStep).ID
            End If
        End If
    Next
    Return -1
End Function

Sub SelectFirstItem()
    Dim firstFound As Boolean
    firstFound = False
    For ItemStep As Integer = Lbound(ItemDats) To Ubound(ItemDats)
        If ItemDats(ItemStep).BoxID = SelectedBoxID() Then
            If firstFound = True Then
                ItemDats(ItemStep).Selected = False
            Else
                ItemDats(ItemStep).Selected = True
                firstFound = True
            End If
        End If
    Next
End Sub

Sub DisplayBoxes(bb() As BoxDat, Focused As Boolean)
    Dim As Integer BoxStep
    For BoxStep = 1 To Ubound(bb)
        LayoutText(bb(BoxStep).Title, bb(BoxStep).Selected, bb(BoxStep).ID, Boxes, TextMode.CENTER, False, Focused, SearchTerm)
        if bb(BoxStep).Selected = True then
            ClearLayout(BoxDescrip)
            LayoutText(bb(BoxStep).Description, False, 0, BoxDescrip, TextMode.LEFT, True, Focused, SearchTerm)
        end if
    Next
End Sub

Sub DisplayItems(ii() As ItemDat, BoxID As Integer, Focused As Boolean)
    Dim ItemOrder As Integer
    ItemOrder = 2
    ClearLayout(BoxItems)
    LayoutText("Qty | Date Added | Title", False, 0, BoxItems, TextMode.LEFT, False, false, SearchTerm)
    LayoutText(String(BoxItems.Width-4, "-"), False, 1, BoxItems, TextMode.LEFT, False, false, SearchTerm)
    For ItemStep As Integer = Lbound(ii) To Ubound(ii)
        If ii(ItemStep).BoxID = BoxID Then
            Dim As String ItemText
            Dim As String Qty
            Qty = ii(ItemStep).Quantity & " "
            if len(qty) < 3 then
                Qty = String(3-len(qty), " ") & Qty
            end if
            ItemText = Qty & " |  " & ii(ItemStep).DateAdded & "  | " & ii(ItemStep).Title
            LayoutText(ItemText, ii(ItemStep).Selected, ItemOrder, BoxItems, TextMode.LEFT, False, Focused, SearchTerm)
            ItemOrder = ItemOrder + 1
        End If
    Next
End Sub