# NIMS

## Nerd Inventory Management System

![alt text](https://gitlab.com/j_pittman/nims/-/raw/main/nims.jpg)

To Compile with FreeBASIC:
```
fbc nims.bas
```

### Usage:

- Tab, Up & Down Arrow moves around
- Escape exits
- Enter selects an Box or Item to edit (In Progress)
More to come!